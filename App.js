import React from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native';
import axios from 'axios';

//url https://api.unsplash.com/photos/random
const APP_ID = 'ae851294d028ac5c51a9e8d2103bfabebd7998c96a70e06a2fed45209f9c90ed';
const APP_SECRET = 'ec022607b273a3e654695c5b3f0431224467ffe79a987bd37a95ed249822ee2b';
const CALLBACK_URL = 'urn:ietf:wg:oauth:2.0:oob';

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            source: {
                uri: null
            },
            creator: "",
            bio: ""
        };
    }

    componentDidMount() {
        this.performSearch();
    }

    performSearch = () => {
        axios
                .get(
                        `https://api.unsplash.com/photos/random/?client_id=${APP_ID}`
                        )
                .then(data => {
                    this.setState({
                        source: {uri: data.data.urls.small},
                        creator: data.data.user.name,
                        bio: data.data.user.bio
                    });
                    Image.getSize(data.data.urls.small, (width, height) => {
                        this.setState({width, height})
                    });
                })
                .catch(err => {
                    console.log('Error happened during fetching!', err);
                });
    }
    ;
            render() {


        return(
                <View style={styles.container}>
                    <Text style={styles.biotext}>{this.state.bio}</Text>
                    <TouchableHighlight onPress={this.performSearch}>
                        <Image 
                            style = {{width: 400, height: 267}}
                            source = { this.state.source }/>
                    </TouchableHighlight>
                    <Text style={styles.nametext}>{this.state.creator}</Text>
                
                </View>

                            );
                }
    }
    
    const styles = StyleSheet.create({
  nametext: {
    color: 'grey',
    fontWeight: 'bold',
    fontSize: 30,
    textAlign: 'center',
    padding: 30
  },
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  biotext:{
    color: 'grey',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    padding: 20
  }
});